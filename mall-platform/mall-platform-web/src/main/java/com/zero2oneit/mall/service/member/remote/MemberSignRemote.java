package com.zero2oneit.mall.service.member.remote;

import com.zero2oneit.mall.common.bean.member.MemberSignRule;
import com.zero2oneit.mall.common.query.member.MemberSignQueryObject;
import com.zero2oneit.mall.common.query.member.MemberSignRecordQueryObject;
import com.zero2oneit.mall.common.query.member.MemberSignRuleQueryObject;
import com.zero2oneit.mall.common.utils.R;
import com.zero2oneit.mall.common.utils.bootstrap.BoostrapDataGrid;
import com.zero2oneit.mall.feign.member.MemberSignFeign;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Description:
 *
 * @author Cmx
 * @date 2021/5/12 19:25
 */
@RestController
@RequestMapping("/remote/sign")
public class MemberSignRemote {

    @Autowired
    private MemberSignFeign signRuleFeign;

    /**
     * 查询签到规则列表信息
     * @param qo
     * @return
     */
    @PostMapping("/ruleList")
    public BoostrapDataGrid list(@RequestBody MemberSignRuleQueryObject qo) {
        return signRuleFeign.ruleList(qo);
    }

    /**
     * 添加或编辑签到规则信息
     * @param memberSignRule
     * @return
     */
    @PostMapping("/addOrEdit")
    public R addOrEdit(MemberSignRule memberSignRule){
        return signRuleFeign.addOrEdit(memberSignRule);
    }

    /**
     * 查询签到记录列表信息
     * @param qo
     * @return
     */
    @PostMapping("/recordList")
    public BoostrapDataGrid recordList(@RequestBody MemberSignRecordQueryObject qo) {
        return signRuleFeign.recordList(qo);
    }

    /**
     * 查询签到列表信息
     * @param qo
     * @return
     */
    @PostMapping("/signList")
    public BoostrapDataGrid signList(@RequestBody MemberSignQueryObject qo) {
        return signRuleFeign.signList(qo);
    }

}
