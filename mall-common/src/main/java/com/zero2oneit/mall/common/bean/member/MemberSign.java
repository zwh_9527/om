package com.zero2oneit.mall.common.bean.member;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * Description:
 *
 * @author Tg
 * @email zero2oneit@163.com
 * @date 2021-05-13
 */
@Data
@TableName("member_sign")
public class MemberSign implements Serializable {

	private static final long serialVersionUID = 1L;

	/**
	 * ID
	 */
	@TableId
	private Long id;
	/**
	 * 会员ID
	 */
	private Long memberId;
	/**
	 * 最后一次签到时间
	 */
	private Date signTime;
	/**
	 * 连续签到次数
	 */
	private Integer signCount;
	/**
	 * 累计签到次数
	 */
	private Integer signAmount;
	/**
	 * 累计签到积分
	 */
	private Integer signPoints;
	/**
	 * 实际签到积分
	 */
	@TableField(exist = false)
	private Integer point;

}
