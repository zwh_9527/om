package com.zero2oneit.mall.member.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zero2oneit.mall.common.bean.member.MemberRetail;
import com.zero2oneit.mall.common.query.member.MemberRetailQueryObject;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

/**
 * @author Tg
 * @create 2021-05-12
 * @description
 */
@Mapper
public interface MemberRetailMapper extends BaseMapper<MemberRetail> {

    int selectTotal(MemberRetailQueryObject qo);

    List<Map<String, Object>> selectAll(MemberRetailQueryObject qo);

}
