package com.zero2oneit.mall.member.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zero2oneit.mall.common.bean.member.CouponRule;
import com.zero2oneit.mall.common.query.member.CouponRuleQueryObject;
import com.zero2oneit.mall.common.query.member.MemberCouponQueryObject;
import com.zero2oneit.mall.common.utils.R;
import com.zero2oneit.mall.common.utils.bootstrap.BoostrapDataGrid;
import com.zero2oneit.mall.member.mapper.CouponRuleMapper;
import com.zero2oneit.mall.member.service.CouponRuleService;
import com.zero2oneit.mall.member.service.MemberCouponService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collections;

/**
 * Description:
 *
 * @author Tg
 * @email zero2oneit@163.com
 * @date 2021-03-19
 */
@Service
public class CouponRuleServiceImpl extends ServiceImpl<CouponRuleMapper, CouponRule> implements CouponRuleService {

    @Autowired
    private CouponRuleMapper couponRuleMapper;

    @Autowired
    private MemberCouponService couponService;

    @Override
    public BoostrapDataGrid pageList(CouponRuleQueryObject qo) {
        int total = couponRuleMapper.selectTotal(qo);
        return new BoostrapDataGrid(total, total ==0 ? Collections.EMPTY_LIST : couponRuleMapper.selectAll(qo));
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public R status(String id, Integer status) {
        couponRuleMapper.status(id, status);
        if(status == 2){
            couponService.status(id, 2);
        }
        return R.ok();
    }

    @Override
    public void reduce(MemberCouponQueryObject qo) {
        couponRuleMapper.reduce(qo);
    }

    @Override
    public R copy(String id) {
        couponRuleMapper.copyRule(id);
        return R.ok();
    }

}